var menuState = {
    create: function(){
        this.background6 = game.add.image(256, 290, 'background1');
        this.land2 = game.add.image(256, 300, 'land');
        this.background5 = game.add.image(256, 160, 'background1');
        this.background4 = game.add.image(256, 38, 'background1');
        this.background3 = game.add.image(0, 290, 'background1');
        this.land = game.add.image(0, 300, 'land');
        this.background2 = game.add.image(0, 160, 'background1');
        this.background = game.add.image(0, 38, 'background1');

        this.player = game.add.sprite(80, 470, 'player');
        this.player.scale.setTo(1.3, 1.3);
        this.player.anchor.setTo(0.5, 0.5);
        this.player.animations.add('idle', [0, 1, 2, 1, 0], 5, true);
        this.player.animations.add('run', [7, 8, 9, 10, 14, 15, 16, 17], 10, true);
        this.player.animations.play('idle');
        this.player.time = 0;
        
        game.time.events.loop(3000,this.menuAnimation,this);

        this.sound_start = game.add.audio('start',0.5);
        this.sound_select = game.add.audio('select', 0.5);
        this.sound_menu = game.add.audio('menu',0.4,true);
        this.sound_menu.play();

        var Namelabel = game.add.text(game.width/2,100,'Down Stairs',{font: '50px slkscrb',fill :'#ffffff'});
        Namelabel.anchor.setTo(0.5,0.5);
        
        this.Menulabel = game.add.text(game.width/2,game.height/2,'Game Start',{
            font: '20px slkscrb',
            fill: '#ffffff'
        });
        this.Menulabel.anchor.setTo(0.5,0.5);
        game.add.tween(this.Menulabel).to({alpha:0},500).yoyo(true).repeat().start();

        this.number = 0;
        var Enter = game.input.keyboard.addKey(Phaser.Keyboard.ENTER);
        var Down = game.input.keyboard.addKey(Phaser.Keyboard.DOWN);
        var Up = game.input.keyboard.addKey(Phaser.Keyboard.UP);
        Enter.onDown.add(this.startgame,this);
        Down.onDown.add(this.Choose,this);
        Up.onDown.add(this.Choose,this);
        
    },
    startgame :function(){
        this.sound_start.play();
        this.sound_menu.stop();
        if(!this.number){
            game.state.start('main');
        }
        else game.state.start('score');
    },
    menuAnimation :function(){
        if(this.player.time % 3 == 0){
            this.player.scale.x = 1.3;
            game.add.tween(this.player).to({x:440},1200).start();
            this.player.animations.play('run');
            game.time.events.add(1200,function(){
                this.player.animations.play('idle');
                this.player.scale.x = -1.3;
            },this);
        }
        else if (this.player.time % 3 == 1){
            this.player.scale.x = -1.3;
            game.add.tween(this.player).to({x:330},300).start();
            this.player.animations.play('run');
            game.time.events.add(300,function(){
                this.player.animations.play('idle');
                this.player.scale.x = -1.3;
            },this);
        }
        else if(this.player.time %3 == 2){
            this.player.scale.x = -1.3;
            game.add.tween(this.player).to({x:80},1000).start();
            this.player.animations.play('run');
            game.time.events.add(1000,function(){
                this.player.animations.play('idle');
                this.player.scale.x = 1.3;
            },this);
        }
        this.player.time++;
    },
    Choose: function(){
        this.sound_select.play();
        if(!this.number){
            this.number = 1;
            this.Menulabel.text = "Scorebroad";
        }
        else{
            this.number = 0;
            this.Menulabel.text = "Game Start";
        }
    }
};
