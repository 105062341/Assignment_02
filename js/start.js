var startState = {
    preload :function(){

        game.load.spritesheet('player', 'assets/UnityChan.png', 64, 64);
        game.load.image('background1', 'assets/BG_01.png');
        game.load.image('land', 'assets/BG_02.png');
        game.load.image('ceiling', 'assets/ceiling.png');
        game.load.spritesheet('block', 'assets/Block.png', 16, 16);
        game.load.image('stair', 'assets/stairs.png');
        game.load.spritesheet('jump', 'assets/jump2.png', 94, 24);
        game.load.image('stab', 'assets/stab.png');
        game.load.image('fake', 'assets/fakestair.png');
        game.load.spritesheet('right', 'assets/right.png', 96, 16);
        game.load.spritesheet('left', 'assets/left.png', 96, 16);

        game.load.audio('hurt', 'assets/audio/univ1091.wav');
        game.load.audio('gameover', 'assets/audio/univ0010.wav');
        game.load.audio('jumping', 'assets/audio/univ0001.wav');
        game.load.audio('select','assets/audio/SE_select.wav');
        game.load.audio('start','assets/audio/SE_start.wav');
        game.load.audio('menu','assets/audio/menu.wav');
        game.load.audio('game','assets/audio/BGM_01_Unitein_the_sky.wav');
    },
    create: function(){
        /*game.stage.backgroundColor = '#000000'; 
        this.loading = game.add.sprite(game.width/2,0,'load');
        this.loading.anchor.setTo(0.5, 0.5);
        game.add.tween(this.loading).to({x: game.width/2,y: game.height/2},800).start();
        game.time.events.add(1100,this.rotate,this);
        game.time.events.add(2100,this.goto,this);
        this.sound = game.add.audio('loading',0.3);*/
        game.state.start('menu');
    }
};