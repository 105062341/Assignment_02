var game = new Phaser.Game(400, 550, Phaser.AUTO, "canvas");

var globalscore;

game.state.add('boot', bootState);
game.state.add('start', startState);
game.state.add('menu', menuState);
game.state.add('score',scoreState);
game.state.add('main', mainState);
game.state.add('over',overState);

game.state.start('boot');