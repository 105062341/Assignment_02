var mainState = {
    preload: function () {
        
    },

    create: function () {
        //background setting
        this.background6 = game.add.image(256, 290, 'background1');
        this.land2 = game.add.image(256, 300, 'land');
        this.background5 = game.add.image(256, 160, 'background1');
        this.background4 = game.add.image(256, 38, 'background1');
        this.background3 = game.add.image(0, 290, 'background1');
        this.land = game.add.image(0, 300, 'land');
        this.background2 = game.add.image(0, 160, 'background1');
        this.background = game.add.image(0, 38, 'background1');
        
        this.ceiling = game.add.sprite(0,0,'ceiling');
        game.physics.arcade.enable(this.ceiling);
        this.ceiling.body.immovable = true;
        this.ceiling.body.setSize(400,6,0,0);
        //player setting
        this.player = game.add.sprite(game.width / 2, game.height / 2, 'player');
        this.player.scale.setTo(1.3,1.3);
        this.player.anchor.setTo(0.5, 0.5);

        game.physics.arcade.enable(this.player);
        this.player.body.setSize(12,50,26,4);
        this.player.body.gravity.y = 350;
        this.player.health = 10;
        this.player.ondamaged = false;
        this.player.lose = false;
        this.player.checkWorldBounds = true;
        this.player.outOfBoundsKill = true;
        this.oncollision = 0;

        this.player.animations.add('idle', [0, 1, 2, 1, 0], 5, true);
        this.player.animations.add('run', [7, 8, 9, 10, 14, 15, 16, 17], 10, true);
        this.player.animations.add('fall', [26, 27], 10, true);
        this.player.animations.add('jump', [21, 22], 10, true);
        this.player.animations.add('transfrom', [23, 24 ,25], 10, false);
        this.player.animations.add('damaged', [3,4], 12, false);
        game.time.events.loop(2500,this.recoverHP,this);
        //walls setting
        this.walls = game.add.group();
        this.walls.enableBody = true; 
        for(var i = 0;i*16<=550;i++){
            game.add.sprite(0,i*16,'block',1,this.walls);
            game.add.sprite(400-16,i*16,'block',1,this.walls); 
        }
        this.walls.scale.setTo(1,1.3);
        game.physics.arcade.enable(this.walls);
        this.walls.setAll('body.immovable',true);

        
        /*this.test = game.add.sprite(game.width/2,game.height/2+50,'block',1);
        this.test.anchor.setTo(0.5,0.5);
        game.physics.arcade.enable(this.test);
        this.test.body.immovable = true;*/
        this.platforms = game.add.group();
        this.platforms.enableBody = true;
        this.level = 0;
        this.platformsCreate();
        game.time.events.loop(1200, this.platformsCreate, this);
        
        game.physics.arcade.enable(this.platforms);

        this.sound_hurt = game.add.audio('hurt',0.5);
        this.sound_gameover = game.add.audio('gameover',0.5);
        this.sound_jumping = game.add.audio('jumping',0.5);
        this.sound_game = game.add.audio('game',0.2,true).play();

        this.HPlabel = game.add.text(50, 50, "HP:"+this.player.health, {
            font: '20px slkscrb',
            fill: '#ffffff'
        });
        this.HPlabel.anchor.setTo(0.5, 0.5);

        this.Levellabel = game.add.text(350,50, "Level" + this.platforms.length, {
            font: '20px slkscrb',
            fill: '#ffffff'
        });
        this.Levellabel.anchor.setTo(0.5,0.5);

        this.cursor = game.input.keyboard.createCursorKeys();
    },
    update: function () {
        this.movePlayer();
        this.platforms.forEachAlive(this.platformsMove,this);
        game.physics.arcade.collide(this.player,this.walls);
        game.physics.arcade.collide(this.player, this.platforms,this.Trigger,null,this);
        game.physics.arcade.collide(this.player,this.ceiling,this.DamagebyCeiling,null,this);
        this.HPlabel.text = 'HP:'+this.player.health;
        this.Levellabel.text = "Level" + this.platforms.length;
        globalscore = this.platforms.length;
        this.checkPlayerDead();
        this.animationsContorl();
    },
    movePlayer: function () {
        
        if (this.cursor.left.isDown) {
            if(this.player.scale.x >0)
                this.player.scale.x *= -1;
            this.player.body.velocity.x += -6;
        } 
        else if (this.cursor.right.isDown) {
            if (this.player.scale.x < 0)
                this.player.scale.x *= -1;
            this.player.body.velocity.x += 6;
        }
        else{
            this.player.body.velocity.x = this.oncollision;
        }
    },
    animationsContorl: function(){
        if(this.player.ondamaged == true) return;
        if(this.player.body.velocity.y == 0){
            if(this.player.body.velocity.x == 0){
                this.player.animations.play('idle');
            }
            else {
                this.player.animations.play('run');
            }
        }
        else{
            this.player.animations.play('fall');
        }
    },
    platformsCreate: function(){
        var platformkey;
        if(this.platforms.length <= 10 )
            platformkey = ['stair', 'stair', 'stair', 'stab', 'jump', 'fake', 'stair'];
        else if(this.platforms.length <=30)
            platformkey = ['stair', 'stair', 'stair', 'stab', 'jump', 'fake', 'fake','left','right'];
        else 
            platformkey = ['stair', 'fake', 'stair', 'stab', 'stab', 'jump', 'fake', 'fake', 'jump', 'left', 'right', 'left', 'right'];
        var key = game.rnd.pick(platformkey);
        var x = game.rnd.between(80,320);
        var y = game.height;
        var platform;
        if(this.level == 0){
            platform = game.add.sprite(game.width/2,game.height,'stair',0,this.platforms);
        }
        else {
            platform = game.add.sprite(x,y,key,0,this.platforms);
            if(platform.key == 'stab')
                platform.body.setSize(96,15,0,10);
            if(platform.key == 'jump')
                platform.animations.add('bounce',[0,1,2,3,4,5,4,3,2,1,0],12,false);
            if (platform.key == 'right'){
                platform.animations.add('rotate_right', [0, 1, 2, 3, 4], 12, true);
                platform.animations.play('rotate_right');
            }
            if (platform.key == 'left'){
                platform.animations.add('rotate_left', [0, 1, 2, 3, 4], 12, true);
                platform.animations.play('rotate_left');
            }
        }
        platform.anchor.setTo(0.5,0.5);
        game.physics.arcade.enable(platform);
        platform.body.immovable = true;
        platform.body.checkCollision.down = false;
        platform.body.checkCollision.left = false;
        platform.body.checkCollision.right = false;
        platform.checkWorldBounds = true;
        platform.outOfBoundsKill = true;
        this.level++;
    },
    platformsMove: function(child){
        if(this.platforms.length <= 15)child.body.position.y -= 2;
        else if(this.platforms.length <= 40)child.body.position.y -= 2.5;
        else child.body.position.y -= 3;
    },
    recoverHP: function(){
        if(this.player.health<10) this.player.heal(1);
    },
    DamagebyCeiling: function(){
        if(this.player.ondamaged == false){
            this.player.body.velocity.y -= 4;
            this.player.animations.play('damaged');
            if(this.platforms.length <=15)this.player.damage(2);
            else this.player.damage(3);
            this.player.ondamaged = true;
            game.time.events.add(800, this.Resetdamage, this);
            game.camera.shake(0.01,200);
            this.sound_hurt.play();
        }
    },
    Resetdamage: function(){
        this.player.ondamaged = false;
    },
    Trigger: function(player,platform){
        if(platform.key == "stair"){
            this.oncollision = 0;
        }
        else if(platform.key == "fake"){
            game.add.tween(platform).to({alpha:0},600).yoyo(true).start();
            platform.body.checkCollision.up = false;
            this.oncollision = 0;
        }
        else if(platform.key == "jump"){
            this.sound_jumping.play();
            platform.animations.play('bounce');
            player.body.velocity.y = -330;
            this.oncollision = 0;
        }
        else if(platform.key == "stab"){
            this.DamagebyCeiling();
            platform.key = 'none';
            this.oncollision = 0;
        }
        else if(platform.key == "right"){
            this.oncollision += 2;
        }
        else if (platform.key == "left") {
            this.oncollision += -2;
        }
    },
    checkPlayerDead: function(){
        if(!this.player.alive && !this.player.lose){
            this.sound_gameover.play();
            this.sound_game.stop();
            this.platforms.setAll('body.checkCollision.up',false);
            this.player.lose = true;
            game.camera.shake(0.01, 300);
            game.camera.flash(0xffffff, 300);
            game.time.events.add(200, function () {
                game.camera.fade(0x000000, 500,true);
            }, this);
            game.time.events.add(700, function () {
                game.state.start('over');
            }, this);
        }
    }
};

//game.state.start('main');
