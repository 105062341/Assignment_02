var overState = {
    create: function(){
        game.stage.backgroundColor = '#000000';
        var OverText = game.add.text(game.width/2, 100, 'Game Over', {
            font: '50px slkscrb',
            fill: '#ffffff'
        });
        OverText.anchor.setTo(0.5, 0.5);
        var ScoreText = game.add.text(game.width / 2, 200, 'Level ' + globalscore, {
            font: '30px slkscrb',
            fill: '#ffffff'
        });
        ScoreText.anchor.setTo(0.5, 0.5);
        this.Restart = game.add.text(game.width / 2,390, 'Restart', {
            font: '20px slkscrb',
            fill: '#ffffff'
        });
        this.Restart.anchor.setTo(0.5, 0.5);
        this.Restarteffect = game.add.tween(this.Restart).to({alpha:0},500).yoyo(true).repeat().start();
        
        this.number = 0;
        this.sound_start = game.add.audio('start', 0.5);
        this.sound_select = game.add.audio('select',0.5);
        game.time.events.add(1000,this.firebaseGet,this);
        var down = game.input.keyboard.addKey(Phaser.Keyboard.DOWN);
        var up = game.input.keyboard.addKey(Phaser.Keyboard.UP);
        var enter = game.input.keyboard.addKey(Phaser.Keyboard.ENTER);          
        down.onDown.add(this.downkey,this);
        up.onDown.add(this.upkey,this);
        enter.onDown.add(this.Get,this);
    },
    downkey: function(){
        this.sound_select.play();
        if (!this.number) {
            this.number++;
            this.Restart.text = "Back to Menu";
        }
        else {
            this.number--;
            this.Restart.text = "Restart";
        }
    },
    upkey: function(){
        this.sound_select.play();
        if (!this.number) {
            this.number++;
             this.Restart.text = "Back to Menu";
        } else {
            this.number--;
            this.Restart.text = "Restart";
        }
    },
    Get: function(){
        this.sound_start.play();
        globalscore = 0;
        if (!this.number) game.state.start('main');
        else game.state.start('menu');
    },
    firebaseGet: function(){
        var name = prompt('Please enter your name');
        if (name != '') {
            var postData = {
                name: name,
                score: globalscore
            };
            var newPostKey = firebase.database().ref().child('game').push().key;
            var updates = {};
            updates['/game/' + newPostKey] = postData;
            firebase.database().ref().update(updates);
        }
    }
};
