var scoreState = {
    create: function(){
        this.GetScore();
        this.First = "";
        this.Second = "";
        this.Third = "";
        this.background6 = game.add.image(256, 290, 'background1');
        this.land2 = game.add.image(256, 300, 'land');
        this.background5 = game.add.image(256, 160, 'background1');
        this.background4 = game.add.image(256, 38, 'background1');
        this.background3 = game.add.image(0, 290, 'background1');
        this.land = game.add.image(0, 300, 'land');
        this.background2 = game.add.image(0, 160, 'background1');
        this.background = game.add.image(0, 38, 'background1');

        this.backlabel = game.add.text(game.width / 2, 400, "Back to Menu", {
            font: '20px slkscrb',
            fill: '#ffffff'
        });
        this.backlabel.anchor.setTo(0.5, 0.5);

        game.add.tween(this.backlabel).to({alpha:0},500).yoyo(true).repeat().start();
        var Enter = game.input.keyboard.addKey(Phaser.Keyboard.ENTER);
        Enter.onDown.add(this.Back,this);
        this.sound_start = game.add.audio('start', 0.5);
        this.sound_menu = game.add.audio('menu', 0.4, true);
        this.sound_menu.play();
    },
    GetScore: function(){
        var i = 0;
        var list = [];
        var scoreRef = firebase.database().ref('game');
        scoreRef.orderByChild('score').limitToLast(3).once('value')
        .then(function(snapshot){
            snapshot.forEach(function(childsnapshot){
                var childData = childsnapshot.val();
                list[i] = childData.name + " " + childData.score;
                i++;
            });
            this.First = list[2];
            this.Second = list[1];
            this.Third = list[0];
            this.scorelabel = game.add.text(game.width / 2, 100, this.First, {
                font: '50px slkscrb',
                fill: '#ffffff'
            });
            this.scorelabel.anchor.setTo(0.5, 0.5);
            this.scorelabel2 = game.add.text(game.width / 2, 200, this.Second, {
                font: '40px slkscrb',
                fill: '#ffffff'
            });
            this.scorelabel2.anchor.setTo(0.5, 0.5);
            this.scorelabel3 = game.add.text(game.width / 2, 300, this.Third, {
                font: '30px slkscrb',
                fill: '#ffffff'
            });
            this.scorelabel3.anchor.setTo(0.5, 0.5);
            console.log(this.First);
            console.log(this.Second);
            console.log(this.Third);
        });
    },
    Back: function(){
        this.sound_menu.stop();
        this.sound_start.play();
        game.state.start('menu');
    }
};