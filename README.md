# Software Studio 2018 Spring Assignment 02 小朋友下樓梯

## 小朋友下樓梯
<img src="example01.png" width="700px" height="500px"></img>

## Goal
1. **Fork the repo ,remove fork relationship and change project visibility to public.**
2. Complete a game "小朋友下樓梯" by Phaser. (JavaScript or TypeScript)
3. Your game should reach the basic requirements.
4. You can download needed materials from some open source webpage to beautify the appearance.
5. Commit to "your" project repository and deploy to Gitlab page.
6. **Report which items you have done and describing other functions or feature in REABME.md.**

## Scoring 
|                                              Item                                              | Score |
|:----------------------------------------------------------------------------------------------:|:-----:|
| A complete game process: start menu => game view => game over => quit or play again            |  20%  |
| Your game should follow the basic rules of  "小朋友下樓梯".                                    |  15%  |
|         All things in your game should have correct physical properties and behaviors.         |  15%  |
| Set up some interesting traps or special mechanisms. .(at least 2 different kinds of platform) |  10%  |
| Add some additional sound effects and UI to enrich your game.                                  |  10%  |
| Store player's name and score in firebase real-time database, and add a leaderboard to your game.        |  10%  |
| Appearance (subjective)                                                                        |  10%  |
| Other creative features in your game (describe on README.md)                                   |  10%  |

## Reminder
* Do not make any change to our root project repository.
* Deploy your web page to Gitlab page, and ensure it works correctly.
    * **Your main page should be named as ```index.html```**
    * **URL should be : https://[studentID].gitlab.io/Assignment_02**
* You should also upload all source code to iLMS.
    * .html or .htm, .css, .js, .ts, etc.
    * source files
* **Deadline: 2018/05/24 23:59 (commit time)**
    * Delay will get 0 point (no reason)
    * Copy will get 0 point
    * "屍體" and 404 is not allowed

# Report
本次作業中，目的是要實作出小朋友下樓梯的遊戲，利用phaser內建的funciton來實作，以下是本次實作出的功能。
這裡列出我所實作的功能：

* 完整遊戲流程
* 符合小朋友下樓梯規則
* 符合物理碰撞
* 不同種類的平台
* 音效和UI
* 美觀動畫
* Scorebroad與Realtime database
* 不同層級的關卡


----
## State
在State中，分成主要四部分，選單模式、遊戲模式、scorebroad模式和結束模式。
* boot state and start state：
在遊戲一開使會先進入boot state和start state，主要負責載入圖片跟音檔和做小動畫的轉場，接著才會進入menu state。
* menu state：
在menu中，玩家可以利用上下鍵和Enter來選擇進入遊戲模式或是scorebroad，此外在menu中也加了小動畫來增加精緻度。
* scorebroad：
在scorebroad中，和firebase的realtime database進行結合，會呈現出目前的前三名及分數，而玩家若是超過前三名分數，名次便會重新排行。
* game state：
game state中主要是遊戲主體的呈現，遊玩方式是利用左右鍵控制角色行進，當死亡是便會進入到結束畫面。
* gameover state：
進入gameover畫面後，視窗便會跳出讓玩家可以輸入名字，並會存到database中，另外在這邊也可以利用上下鍵來決定是否要重來或是回到主選單看scorebroad。

----
## Game rule
在遊戲規則方面，採用與小朋友下樓梯一樣的規則，玩家控制角色在針刺天花版的追趕下不斷往下爬，當血量歸零時或是角色掉落時，遊戲便會結束。

在遊戲設置方面，我利用phaser的`health`功能，將玩家血量設置在10點，當health歸零時，phaser便會自動`kill`玩家，另外每2.5秒便會回血。

角色血量設置和回血功能：
```js
this.player.health = 10;
game.time.events.loop(2500,this.recoverHP,this);
recoverHP: function(){
    if(this.player.health<10) this.player.heal(1);
}
```
傷害造成：
```js
this.player.damage(3);
```
----
## Physics
在物理碰撞方面，我使用了ARCADE去進行物理上偵測，利用`collide()`來進行碰撞function的進行，另外由於一開始在設置物體碰撞大小時，是以spritesheet大小來當預設值，所以必須利用`setSiz()`來修改碰撞大小以符合角色形狀。
```js
this.player.body.setSize(12,50,26,4);
```
而在碰撞方面，設置了天花板的碰撞和平台的碰撞，利用了callback來對不同的case處理。

---
## Platforms
本次實作設置了許多不同功能的平台，分為一般、針刺、輸送帶、穿透、彈簧。

在實作中，由於必須同時管理這些平台，因此我利用group的方式來管理所有平台，由於平台得種類必須是隨機的，而x的位置同樣也是隨機的，因此我利用了`rnd`的`pick()`和`between()`來處理，在`pick()`方面，我將各種不同的平台的key設置在一個陣列中，如此一來便可以隨機取出不同的keu，而`between`則是設置一個範圍內隨機取出值。

另外在這邊統一將所有平台的`checkCollision`設置成只針對上方，且`outOfBoundsKill`設置成true，便可以利用`forEachAlive`來針對所有活著的平台進行移動處理。

最後根據不同的key，在跟player的collision中利用callback function設置不同的效果，並撥放對應的動畫

----
## Sounds and UI
利用了phaser內建的audio設置，來對各種聲音進行配置。
```
game.load.audio();
```
----
## Scorebroad
在scoreborad方面，和firebase的realtime database進行結合，利用`orderByChild()`以分數來進行排序，並利用`limitToLast()`來取得前三名，如此便能完成scorebroad的實作。

----
## 不同level的關卡（other function）
在這邊我利用Score大小，對難易度進行調整，讓整體遊戲增加可玩性，隨著分數增加的一定大小，讓平台種類變形，並讓平台速度上升，甚至是增加針刺造成的傷害

